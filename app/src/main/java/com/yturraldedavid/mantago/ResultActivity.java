package com.yturraldedavid.mantago;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;


public class ResultActivity extends AppCompatActivity implements  RecyclerView.OnItemTouchListener{

    RecyclerView recyclerViewLugares;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        recyclerViewLugares = (RecyclerView) findViewById(R.id.recyclerViewLugares);
        recyclerViewLugares.setLayoutManager(new LinearLayoutManager(this));
        final ArrayList<LugaresTuristicos> listaLugares = new ArrayList<>();
        final ArrayList<MapaLugar> latlng = new ArrayList<>();
        int opcion = getIntent().getExtras().getInt("opcion");
        switch (opcion){
            case 1:
                listaLugares.add(new LugaresTuristicos("Museo Cancebí",R.drawable.lt01, "Museo caracteristico de manabi", 0));
                latlng.add(new MapaLugar(-0.947417, -80.721746));
                listaLugares.add(new LugaresTuristicos("Museo Centro Cultural Manta",R.drawable.lt02, "Museo caracteristico de manabi", 17));
                latlng.add(new MapaLugar(-0.947417, -80.721746));
                break;
            case 2:
                listaLugares.add(new LugaresTuristicos("Playa Murcielago", R.drawable.p01,"Playa principal de la ciudad", 1));
                latlng.add(new MapaLugar( -0.939401, -80.730193));
                listaLugares.add(new LugaresTuristicos("Playita Mia", R.drawable.p02,"Playa característica por su venta de pescados", 2));
                latlng.add(new MapaLugar(-0.949447, -80.709220));
                break;

            case 3:
                listaLugares.add(new LugaresTuristicos("Martinica", R.drawable.r01,"Restaurante con un toque fino ", 3));
                latlng.add(new MapaLugar(-0.945878, -80.745170));
                listaLugares.add(new LugaresTuristicos("Chamaco", R.drawable.r02,"Restaurante enfocado a la comida mexicana", 4));
                latlng.add(new MapaLugar(-0.946947,-80.745965));
                listaLugares.add(new LugaresTuristicos("El Faro", R.drawable.r03,"Ubicado a pocos metros del nuevo centro comercial y de la playa, ofrece un ambiente especial para todo tipo de ocasión. Vista del Océano y el clima típico de Manta son la combinación ideal. Atención cordial, una cartilla completa con interesantes opciones de bebidas y piqueos fuera de lo tradicional.", 5));
                latlng.add(new MapaLugar(-0.946947,-80.745965));
                listaLugares.add(new LugaresTuristicos("Mamma Rosa", R.drawable.r04,"Cuando de comida italiana se trata, en Manta no hay mejor lugar que Mamma Rosa. Presente ya alrededor de 2 décadas, este icónico lugar ha sabido ganar su espacio gracias a la perseverancia de su equipo de trabajo e impecable decoración que lo transportan a uno a la mismísima Italia.", 6));
                latlng.add(new MapaLugar(-0.946947,-80.745965));
                listaLugares.add(new LugaresTuristicos("Krug", R.drawable.r05,"Un lugar hecho para el encuentro de amigos sin cita previa necesaria. Su combinación de música, comida y buena onda, hacen que sea rico pasar mucho tiempo aquí. ", 7));
                latlng.add(new MapaLugar(-0.946947,-80.745965));
                listaLugares.add(new LugaresTuristicos("Los 3 Platos", R.drawable.r06,"El mejor sitio para disfrutar con tus familiares y amigos. Encontrarás gran variedad de platos hechos a la parrilla, deliciosas ensaladas, jugos, postres, cocteles y sin lugar a dudas: LA MEJOR PIZZA de la ciudad de Manta en Ecuador.Ademas de deleitarte con comida de la mejor calidad, te ofrecemos un ambiente cómodo, con terraza, parqueadero y mucha creatividad!Te esperamos en LOS 3 PLATOS!!!! ", 8));
                latlng.add(new MapaLugar(-0.946947,-80.745965));
                break;

            case 4:
                listaLugares.add(new LugaresTuristicos("Wild Bar", R.drawable.b01,"Este bar es frecuentado por gente joven", 9));
                latlng.add(new MapaLugar(-0.945044, -80.735862));
                listaLugares.add(new LugaresTuristicos("Noa Bar", R.drawable.b02,"Bar capaz de otorgar una musica agradable",10 ));
                latlng.add(new MapaLugar(-0.945188, -80.731077));
                listaLugares.add(new LugaresTuristicos("Molotov", R.drawable.b03,"Bar que ofrece buena musica y bebida para disfrutar en compañia de amigos",18 ));
                latlng.add(new MapaLugar(-0.945188, -80.731077));
                listaLugares.add(new LugaresTuristicos("MAGIC", R.drawable.b04,"Bar capaz de otorgar una musica agradable",19 ));
                latlng.add(new MapaLugar(-0.945188, -80.731077));

                break;
            case 5:
                listaLugares.add(new LugaresTuristicos("Oro verde", R.drawable.h01,"Hotel mas conocido de la ciudad", 11));
                latlng.add(new MapaLugar(-0.941511, -80.732737));
                listaLugares.add(new LugaresTuristicos("Balandra", R.drawable.h02,"Hotel barato y comodo", 12));
                latlng.add(new MapaLugar(-0.942486, -80.730536));
                listaLugares.add(new LugaresTuristicos("Hotel Boutique Maria Isabel", R.drawable.h03, "A tan solo 5 Minutos de la Playa el Murcielago y a 10 del Aeropuerto de Manta se encuentra nuestro hotel contando con 16 habitaciones de tipo doble, matrimonial, triple, familiar, y presidencial. Áreas verdes, garaje privado, en donde el huésped visitante no solo encuentre la atención y satisfacción a sus necesidades, sano esparcimiento y diversión sino que también pueda adquirir mediante su compra el bien mueble, equipos o pinturas que le sean de su agrado dentro del hotel.", 13));
                latlng.add(new MapaLugar(-0.942486, -80.730536));
                listaLugares.add(new LugaresTuristicos("Mantahost Hotel", R.drawable.h04,"Es uno de los hoteles emblemáticos de la ciudad de Manta que se distingue por ser un cálido destino y por la calidad del servicio ofrecida a nuestros clientes nacionales y extranjeros, con una experiencia y reconocimiento por 10 años en el mercado.", 14));
                latlng.add(new MapaLugar(-0.942486, -80.730536));
                listaLugares.add(new LugaresTuristicos("Hotel Perla Spondylus", R.drawable.h05,"El Hotel Perla Spondylus esta ubicado en el corazon de Manta, a 100 metros de la zona financiera y a 500 metros de la Playa El Murcielago y el Museo Cultural Manta. Esta a 10 minutos en automovil del Aeropuerto Eloy Alfaro y a 30 minutos de Montecristi. Ofrece desayuno, wifi y parqueo gratuido. Confortables suites y habitaciones. Algunas de ellas con amplias ventanas con vista al mar.", 15));
                latlng.add(new MapaLugar(-0.941511, -80.732737));
                listaLugares.add(new LugaresTuristicos("Hotel Los Almendros", R.drawable.h06,"Hotel cerca de todo ,especial si viajas por negocios, cómodo y elegante con una velocidad alta a internet gratis . Si te gusta hacer ejercicios por la mañana o nadar puedes hacerlo en nuestro gimnasio o piscina . Desayuna en nuestro restaurante desde las 6:30 am y no te preocupes porque está incluido.", 16));
                latlng.add(new MapaLugar(-0.941511, -80.732737));

        }


        
        ResultListAdapter adapter = new ResultListAdapter(listaLugares);
        adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id= listaLugares.get
                        (recyclerViewLugares.getChildAdapterPosition(v))
                        .getId();

                if (id == 0) {

                    Intent intent = new Intent(ResultActivity.this, DetailActivity.class);
                    intent.putExtra("ID", id);
                    intent.putExtra("nombreLugar", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getNombre());
                    intent.putExtra("descripcion", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getDescripcion());
                    intent.putExtra("foto",  listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getFoto());
                    intent.putExtra("Lat", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLat());
                    intent.putExtra("Lng", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLng());
                    startActivity(intent);
                } else if (id == 1) {

                    Intent intent = new Intent(ResultActivity.this, DetailActivity.class);
                    intent.putExtra("ID", id);
                    intent.putExtra("nombreLugar", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getNombre());
                    intent.putExtra("descripcion", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getDescripcion());
                    intent.putExtra("foto",  listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getFoto());
                    intent.putExtra("Lat", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLat());
                    intent.putExtra("Lng", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLng());
                    startActivity(intent);
                } else if (id == 2) {

                    Intent intent = new Intent(ResultActivity.this, DetailActivity.class);
                    intent.putExtra("ID", id);
                    intent.putExtra("nombreLugar", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getNombre());
                    intent.putExtra("descripcion", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getDescripcion());
                    intent.putExtra("foto",  listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getFoto());
                    intent.putExtra("Lat", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLat());
                    intent.putExtra("Lng", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLng());
                    startActivity(intent);
                } else if (id == 3) {

                    Intent intent = new Intent(ResultActivity.this, DetailActivity.class);
                    intent.putExtra("ID", id);
                    intent.putExtra("nombreLugar", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getNombre());
                    intent.putExtra("descripcion", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getDescripcion());
                    intent.putExtra("foto",  listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getFoto());
                    intent.putExtra("Lat", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLat());
                    intent.putExtra("Lng", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLng());
                    startActivity(intent);
                } else if (id == 4) {

                    Intent intent = new Intent(ResultActivity.this, DetailActivity.class);
                    intent.putExtra("ID", id);
                    intent.putExtra("nombreLugar", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getNombre());
                    intent.putExtra("descripcion", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getDescripcion());
                    intent.putExtra("foto",  listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getFoto());
                    intent.putExtra("Lat", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLat());
                    intent.putExtra("Lng", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLng());
                    startActivity(intent);
                }else if (id == 5) {

                    Intent intent = new Intent(ResultActivity.this, DetailActivity.class);
                    intent.putExtra("ID", id);
                    intent.putExtra("nombreLugar", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getNombre());
                    intent.putExtra("descripcion", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getDescripcion());
                    intent.putExtra("foto",  listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getFoto());
                    intent.putExtra("Lat", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLat());
                    intent.putExtra("Lng", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLng());
                    startActivity(intent);
                }else if (id == 6) {

                    Intent intent = new Intent(ResultActivity.this, DetailActivity.class);
                    intent.putExtra("ID", id);
                    intent.putExtra("nombreLugar", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getNombre());
                    intent.putExtra("descripcion", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getDescripcion());
                    intent.putExtra("foto",  listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getFoto());
                    intent.putExtra("Lat", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLat());
                    intent.putExtra("Lng", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLng());
                    startActivity(intent);
                }else if (id == 7) {

                    Intent intent = new Intent(ResultActivity.this, DetailActivity.class);
                    intent.putExtra("ID", id);
                    intent.putExtra("nombreLugar", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getNombre());
                    intent.putExtra("descripcion", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getDescripcion());
                    intent.putExtra("foto",  listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getFoto());
                    intent.putExtra("Lat", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLat());
                    intent.putExtra("Lng", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLng());
                    startActivity(intent);
                }else if (id == 8) {

                    Intent intent = new Intent(ResultActivity.this, DetailActivity.class);
                    intent.putExtra("ID", id);
                    intent.putExtra("nombreLugar", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getNombre());
                    intent.putExtra("descripcion", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getDescripcion());
                    intent.putExtra("foto",  listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getFoto());
                    intent.putExtra("Lat", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLat());
                    intent.putExtra("Lng", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLng());
                    startActivity(intent);
                }else if (id == 9) {

                    Intent intent = new Intent(ResultActivity.this, DetailActivity.class);
                    intent.putExtra("ID", id);
                    intent.putExtra("nombreLugar", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getNombre());
                    intent.putExtra("descripcion", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getDescripcion());
                    intent.putExtra("foto",  listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getFoto());
                    intent.putExtra("Lat", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLat());
                    intent.putExtra("Lng", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLng());
                    startActivity(intent);
                }else if (id == 10) {

                    Intent intent = new Intent(ResultActivity.this, DetailActivity.class);
                    intent.putExtra("ID", id);
                    intent.putExtra("nombreLugar", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getNombre());
                    intent.putExtra("descripcion", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getDescripcion());
                    intent.putExtra("foto",  listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getFoto());
                    intent.putExtra("Lat", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLat());
                    intent.putExtra("Lng", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLng());
                    startActivity(intent);
                }else if (id ==11) {

                    Intent intent = new Intent(ResultActivity.this, DetailActivity.class);
                    intent.putExtra("ID", id);
                    intent.putExtra("nombreLugar", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getNombre());
                    intent.putExtra("descripcion", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getDescripcion());
                    intent.putExtra("foto",  listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getFoto());
                    intent.putExtra("Lat", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLat());
                    intent.putExtra("Lng", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLng());
                    startActivity(intent);
                }else if (id == 12) {

                    Intent intent = new Intent(ResultActivity.this, DetailActivity.class);
                    intent.putExtra("ID", id);
                    intent.putExtra("nombreLugar", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getNombre());
                    intent.putExtra("descripcion", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getDescripcion());
                    intent.putExtra("foto",  listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getFoto());
                    intent.putExtra("Lat", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLat());
                    intent.putExtra("Lng", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLng());
                    startActivity(intent);
                }else if (id == 13) {

                    Intent intent = new Intent(ResultActivity.this, DetailActivity.class);
                    intent.putExtra("ID", id);
                    intent.putExtra("nombreLugar", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getNombre());
                    intent.putExtra("descripcion", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getDescripcion());
                    intent.putExtra("foto",  listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getFoto());
                    intent.putExtra("Lat", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLat());
                    intent.putExtra("Lng", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLng());
                    startActivity(intent);
                }else if (id == 14) {

                    Intent intent = new Intent(ResultActivity.this, DetailActivity.class);
                    intent.putExtra("ID", id);
                    intent.putExtra("nombreLugar", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getNombre());
                    intent.putExtra("descripcion", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getDescripcion());
                    intent.putExtra("foto",  listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getFoto());
                    intent.putExtra("Lat", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLat());
                    intent.putExtra("Lng", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLng());
                    startActivity(intent);
                }else if (id == 15) {

                    Intent intent = new Intent(ResultActivity.this, DetailActivity.class);
                    intent.putExtra("ID", id);
                    intent.putExtra("nombreLugar", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getNombre());
                    intent.putExtra("descripcion", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getDescripcion());
                    intent.putExtra("foto",  listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getFoto());
                    intent.putExtra("Lat", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLat());
                    intent.putExtra("Lng", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLng());
                    startActivity(intent);
                }else if (id == 16) {

                    Intent intent = new Intent(ResultActivity.this, DetailActivity.class);
                    intent.putExtra("ID", id);
                    intent.putExtra("nombreLugar", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getNombre());
                    intent.putExtra("descripcion", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getDescripcion());
                    intent.putExtra("foto",  listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getFoto());
                    intent.putExtra("Lat", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLat());
                    intent.putExtra("Lng", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLng());
                    startActivity(intent);
                }else if (id == 17) {

                    Intent intent = new Intent(ResultActivity.this, DetailActivity.class);
                    intent.putExtra("ID", id);
                    intent.putExtra("nombreLugar", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getNombre());
                    intent.putExtra("descripcion", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getDescripcion());
                    intent.putExtra("foto",  listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getFoto());
                    intent.putExtra("Lat", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLat());
                    intent.putExtra("Lng", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLng());
                    startActivity(intent);
                }else if (id == 18) {

                    Intent intent = new Intent(ResultActivity.this, DetailActivity.class);
                    intent.putExtra("ID", id);
                    intent.putExtra("nombreLugar", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getNombre());
                    intent.putExtra("descripcion", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getDescripcion());
                    intent.putExtra("foto",  listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getFoto());
                    intent.putExtra("Lat", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLat());
                    intent.putExtra("Lng", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLng());
                    startActivity(intent);
                }else if (id == 19) {

                    Intent intent = new Intent(ResultActivity.this, DetailActivity.class);
                    intent.putExtra("ID", id);
                    intent.putExtra("nombreLugar", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getNombre());
                    intent.putExtra("descripcion", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getDescripcion());
                    intent.putExtra("foto",  listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getFoto());
                    intent.putExtra("Lat", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLat());
                    intent.putExtra("Lng", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLng());
                    startActivity(intent);
                }


                Toast.makeText(getApplicationContext(), "Selección: "+listaLugares.get
                        (recyclerViewLugares.getChildAdapterPosition(v))
                        .getNombre(),Toast.LENGTH_SHORT).show();
            }
        });
        recyclerViewLugares.setAdapter(adapter);

    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    public interface  OnItemClickListener {
        public void OnItemClickListener(View view, int position);


    }



}
